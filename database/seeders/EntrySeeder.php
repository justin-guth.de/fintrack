<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Category;
use App\Models\Entry;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class EntrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account1 = Account::where("email", "fintrack.tester1@example.com")->firstOrFail()->id;
        $account2 = Account::where("email", "fintrack.tester2@example.com")->firstOrFail()->id;

        $categoryGifts = Category::where("name", "Gifts")->firstOrFail()->id;
        $categoryInvestmentReturn = Category::where("name", "Investment Return")->firstOrFail()->id;
        $categoryDividends = Category::where("name", "Dividends")->firstOrFail()->id;
        $categorySales = Category::where("name", "Sales")->firstOrFail()->id;
        $categoryPaychecks = Category::where("name", "Paychecks")->firstOrFail()->id;
        $categoryLeisure = Category::where("name", "Leisure")->firstOrFail()->id;
        $categoryRestaurant = Category::where("name", "Restaurants")->firstOrFail()->id;
        $categoryHoliday = Category::where("name", "Holiday")->firstOrFail()->id;
        $categoryNecessities = Category::where("name", "Necessities")->firstOrFail()->id;
        $categoryRent = Category::where("name", "Rent")->firstOrFail()->id;
        $categoryUniversity = Category::where("name", "University")->firstOrFail()->id;
        $categoryIncome = Category::where("name", "Income")->firstOrFail()->id;
        $categoryExpenses = Category::where("name", "Expenses")->firstOrFail()->id;

        $data = [
            ["Paycheck July", "My first paycheck", 10000, $categoryPaychecks, Carbon::createFromDate(2021, 7, 30), $account1],
            ["Paycheck August", "My second paycheck", 15000, $categoryPaychecks, Carbon::createFromDate(2021, 8, 30), $account1],
            ["Summer Bonus", null, 5000, $categoryPaychecks, Carbon::createFromDate(2021, 7, 15), $account1],
            ["Restaurant visit", "Pub", 3000, $categoryLeisure, Carbon::createFromDate(2021, 6, 12), $account1],
            ["Cinema", "Pulp Fiction", 1500, $categoryLeisure, Carbon::createFromDate(2021, 1, 1), $account1],
            ["Groceries", null, 6000, $categoryNecessities, Carbon::createFromDate(2021, 5, 10), $account1],
            ["Dividends", null, 9000, $categoryDividends, Carbon::createFromDate(2021, 7, 11), $account1],
            ["Something", null, 9000, $categoryInvestmentReturn, Carbon::createFromDate(2021, 7, 11), $account1],
            ["Sales", null, 9000, $categorySales, Carbon::createFromDate(2021, 8, 11), $account1],
            ["Restaurant", null, 9000, $categoryRestaurant, Carbon::createFromDate(2021, 8, 11), $account1],
            ["Holiday", null, 9000, $categoryHoliday, Carbon::createFromDate(2021, 8, 11), $account1],
            ["Rent", null, 9000, $categoryRent, Carbon::createFromDate(2021, 8, 11), $account1],
            ["University", null, 9000, $categoryUniversity, Carbon::createFromDate(2021, 8, 11), $account1],
            ["Grandma's gift", null, 5000, $categoryGifts, Carbon::createFromDate(2021, 6, 27), $account2],
        ];


        foreach($data as $dataset) {

            Entry::create(
                [
                    "title" => $dataset[0],
                    "description" => $dataset[1],
                    "amount" => $dataset[2],
                    "category_id" => $dataset[3],
                    "date" => $dataset[4],
                    "account_id" => $dataset[5],
                ]
            );
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFor(Account::whereEmail("fintrack.tester1@example.com")->first()->id);
        $this->seedFor(Account::whereEmail("fintrack.tester2@example.com")->first()->id);
    }
    public function seedFor($account_id)
    {
        $expenses = Category::create(

            [
                "name" => "Expenses",
                "incoming" => false,
                "parent_id" => null,
                "account_id"=>$account_id,
            ]
        );

        $income = Category::create(
            [
                "name" => "Income",
                "incoming" => true,
                "parent_id" => null,
                "account_id"=>$account_id,
            ]
        );

        $necessities = Category::create(
            [
                "account_id"=>$account_id,
                "name" => "Necessities",
                "parent_id" => $expenses->id,
                "incoming" => false
            ]
        );


        Category::create(
            [
                "account_id"=>$account_id,
                "name" => "Groceries",
                "parent_id" => $necessities->id,
                "incoming" => false
            ]
        );

        Category::create(
            [
                "account_id"=>$account_id,
                "name" => "University",
                "parent_id" => $necessities->id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "account_id"=>$account_id,
                "name" => "Insurance",
                "parent_id" => $necessities->id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "account_id"=>$account_id,
                "name" => "Rent",
                "parent_id" => $necessities->id,
                "incoming" => false,
            ]
        );

        $leisure = Category::create(
            [
                "name" => "Leisure",
                "account_id"=>$account_id,
                "parent_id" => $expenses->id,
                "incoming" => false,
            ]
        );

        $goingOut = Category::create(
            [
                "name" => "Goining out",
                "parent_id" => $leisure->id,
                "account_id"=>$account_id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "account_id"=>$account_id,
                "name" => "Games",
                "parent_id" => $leisure->id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "name" => "Restaurants",
                "account_id"=>$account_id,
                "parent_id" => $goingOut->id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "name" => "Takeaway",
                "parent_id" => $goingOut->id,
                "account_id"=>$account_id,
                "incoming" => false,
            ]
        );


        Category::create(
            [
                "name" => "Holiday",
                "parent_id" => $leisure->id,
                "account_id"=>$account_id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "name" => "Home improvement",
                "parent_id" => $leisure->id,
                "account_id"=>$account_id,
                "incoming" => false,
            ]
        );

        $pc = Category::create(
            [
                "name" => "Paychecks",
                "parent_id" => $income->id,
                "account_id"=>$account_id,
                "incoming" => true,
            ]
        );

        Category::create(
            [
                "name" => "Paybacks",
                "account_id"=>$account_id,
                "parent_id" => $income->id,
                "incoming" => true,
            ]
        );

        Category::create(
            [
                "name" => "Normal paychecks",
                "parent_id" => $pc->id,
                "account_id"=>$account_id,
                "incoming" => true,
            ]
        );

        Category::create(
            [
                "name" => "Bonus",
                "account_id"=>$account_id,
                "parent_id" => $pc->id,
                "incoming" => true,
            ]
        );

        Category::create(
            [
                "name" => "Gifts",
                "parent_id" => $income->id,
                "account_id"=>$account_id,
                "incoming" => true,
            ]
        );
        $investments = Category::create(
            [
                "name" => "Investment return",
                "account_id"=>$account_id,
                "parent_id" => $income->id,
                "incoming" => true,
            ]
        );

        Category::create(
            [
                "name" => "Dividends",
                "parent_id" => $investments->id,
                "account_id"=>$account_id,
                "incoming" => true,
            ]
        );

        Category::create(
            [
                "name" => "Sales",
                "parent_id" => $investments->id,
                "account_id"=>$account_id,
                "incoming" => true,
            ]
        );


        $savings = Category::create(
            [
                "name" => "Savings",
                "account_id"=>$account_id,
                "parent_id" => $expenses->id,
                "incoming" => false,
            ]
        );

        $etf = Category::create(
            [
                "name" => "ETF",
                "parent_id" => $savings->id,
                "account_id"=>$account_id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "name" => "ETF Plan",
                "parent_id" => $etf->id,
                "account_id"=>$account_id,
                "incoming" => false,
            ]
        );

        Category::create(
            [
                "account_id"=>$account_id,
                "name" => "ETF Individual",
                "parent_id" => $etf->id,
                "incoming" => false,
            ]
        );
        Category::create(
            [
                "name" => "Stocks",
                "parent_id" => $savings->id,
                "account_id"=>$account_id,
                "incoming" => false,
            ]
        );
    }
}

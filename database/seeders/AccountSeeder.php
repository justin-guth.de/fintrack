<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::create(
            [
                "name" => "tester1",
                "email" => "fintrack.tester1@example.com",
                "password_hash" => Hash::make("tester123"),
            ]
        );
        Account::create(
            [
                "name" => "tester2",
                "email" => "fintrack.tester2@example.com",
                "password_hash" => Hash::make("tester123"),
            ]
        );
    }
}

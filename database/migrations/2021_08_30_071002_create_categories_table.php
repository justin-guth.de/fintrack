<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("name", 256);
            $table->boolean("incoming");
            $table->uuid("parent_id")
                ->nullable()
                ->constrained()
                ->default(null);
            $table->string("color", 7)->nullable()->default(null);
            $table->string("icon")->nullable()->default(null);
            $table->foreignUuid("account_id")->nullable()->references("id")->on("accounts")->onDelete("CASCADE")->onUpdate("CASCADE")->default(null);
            $table->timestamps();
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign("parent_id")
                ->references("id")
                ->on("categories")
                ->onDelete("SET NULL")
                ->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

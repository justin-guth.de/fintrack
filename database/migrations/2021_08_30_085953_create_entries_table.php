<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->uuid("id")->primary();


            $table->string("title", 256)->nullable()->default(null);
            $table->string("description", 2048)->nullable()->default(null);

            $table->foreignUuid('category_id')
                ->nullable()
                ->constrained()
                ->default(null)
                ->references('id')
                ->on('categories')
                ->onDelete("SET NULL")
                ->onUpdate("CASCADE");

            $table->foreignUuid('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete("CASCADE")
                ->onUpdate('CASCADE');

            $table->date("date");
            $table->integer("amount");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}

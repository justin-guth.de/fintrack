<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkTagsEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_tags_entries', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid("tag_id")->references("id")->on("tags")->onDelete("cascade");
            $table->foreignUuid("entry_id")->references("id")->on("entries")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_tags_entries');
    }
}

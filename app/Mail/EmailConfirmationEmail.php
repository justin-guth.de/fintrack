<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailConfirmationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("[FinTrack] Please verify your email address")
        // ->bcc(env('MAIL_BCC_ADDRESS', 'fintrack@justin-guth.de'), "FinTrack")
        ->markdown('emails.email_confirmation')
        ->with([
            "url" => env('WEB_URL') . "/verify_email/" . $this->token,
        ]);
    }
}

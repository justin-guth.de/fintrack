<?php

namespace App\Models;

use App\Utility\HSVToRGB;
use Carbon\Carbon;
use Exception;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    use HasFactory;
    use Uuid;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "tag",
        "color",
    ];

    public function toArray()
    {

        return [

            "id" => $this->id,
            "tag" => $this->tag,
            "color" => $this->color,
        ];
    }

    public function linkEntry($entryId)
    {
        $link = DB::table('link_tags_entries')
            ->where("tag_id", "=", $this->id)
            ->where("entry_id", "=", $entryId)->first();

        if ($link != null) {
            return;
        }

        $created = DB::table('link_tags_entries')->insert(
            [
                "tag_id" => $this->id,
                "entry_id" => $entryId,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
    }

    public function unlinkEntry($entryId)
    {

        $link = DB::table('link_tags_entries')
            ->where("tag_id", "=", $this->id)
            ->where("entry_id", "=", $entryId)->first();

        if ($link == null) {

            throw new Exception("No link found to unlink.");
        }

        DB::table("link_tags_entries")->delete($link->id);
    }

    public static function getOrBuild($tag)
    {

        $existingTag = Tag::firstWhere("tag", "=", $tag);

        if ($existingTag == null) {

            $hue = mt_rand() / mt_getrandmax();
            $saturation = (0.4 + 0.6 * (mt_rand() / mt_getrandmax()));
            $value = (0.4 + 0.6 * (mt_rand() / mt_getrandmax()));

            $existingTag = Tag::create(
                [
                    "tag" => $tag,
                    "color" => HSVToRGB::hsvToRgb($hue, $saturation, $value),
                ]
            );
        }

        return $existingTag;
    }

    public static function findIn($text)
    {

        if ($text == null) {

            return [];
        }

        $matchCount = preg_match_all(
            "/#(?!\\S*#)(\\S+)(\\s|$)/", // Pattern to look for
            $text, // Subject
            $matches
        );

        if ($matchCount != false) {

            $tags = [];

            foreach ($matches[0] as $match) {
                $tags[] = trim($match);
            }

            return $tags;
        }

        return [];
    }
}

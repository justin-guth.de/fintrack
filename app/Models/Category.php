<?php

namespace App\Models;

use Exception;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Uuid;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "parent_id",
        "incoming",
        "name",
        "account_id",
        "color",
        "icon"
    ];

    protected $table = "categories";

    protected $casts = [
        "incoming" => "boolean",
    ];

    public function toArray()
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "parent_id" => $this->parent_id,
            "is_incoming" => $this->incoming,
            "color" => $this->color,
            "icon" => $this->icon
        ];
    }
}

<?php

namespace App\Models;

use App\Utility\CurrencyFormatter;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use Uuid;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "category_id",
        "title",
        "description",
        "date",
        "amount",
        "account_id",
    ];

    protected $table = "entries";

    public function category()
    {

        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function tags()
    {

        return $this->belongsToMany(Tag::class, "link_tags_entries", "entry_id", "tag_id");
    }

    public function recomputeTags()
    {
        // Find all tags in entry

        $tags = Tag::findIn($this->description);
        $currentTags = $this->tags;

        foreach ($currentTags as $currentTag) {

            if (!in_array($currentTag->tag, $tags)) {

                $currentTag->unlinkEntry($this->id);
            }
        }

        foreach ($tags as $tag) {

            $existingTag = Tag::getOrBuild($tag);
            $existingTag->linkEntry($this->id);
        }
    }

    public function toArray()
    {
        $tags = $this->tags->map(function ($tag) {
            return $tag->toArray();
        });

        return [

            "id" => $this->id,
            "category_name" => $this->category->name,
            "amount_representation" => CurrencyFormatter::formatEuro($this->amount),
            "category_id" => $this->category_id,
            "title" => $this->title,
            "description" => $this->description,
            "date" => $this->date,
            "amount" => $this->amount,
            "absolute_amount" => abs($this->amount),
            "account_id" => $this->account_id,
            "category_color" => $this->category->color,
            "category_icon" => $this->category->icon,
            "tags" => $tags,
        ];
    }
}

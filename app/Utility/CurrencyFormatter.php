<?php

namespace App\Utility;

use App\Exceptions\CustomBaseException;
use Illuminate\Http\JsonResponse;

class CurrencyFormatter
{


    public static function formatEuro($cents) {

        $isNegative = $cents < 0;
        $cents = abs($cents);

        if ($cents < 10) {
            return "0,0" . $cents . "€";
        }else if ($cents < 100) {
            return "0," . $cents . "€";
        } else {

            $centPart = "". $cents % 100;
            if (strlen($centPart) == 1) {
                $centPart = "0" . $centPart;
            }
            return ($isNegative ? "-" : "") . intdiv($cents, 100) . "," . $centPart . "€";
        }
    }
}

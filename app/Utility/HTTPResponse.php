<?php

namespace App\Utility;

use App\Exceptions\CustomBaseException;
use Illuminate\Http\JsonResponse;

class HTTPResponse
{


    /**
     * @param int   $code The error code for the response
     * @param array $data the data to be put into the response
     *
     * @return JsonResponse the resulting response
     */
    public static function code(int $code, array $data = []): JsonResponse
    {
        if ($code >= 300) {
            return response()->json([
                "is_error" => true,
                "errors" => $data
            ])->setStatusCode($code);
        }

        return response()->json([
            "is_error" => false,
            "data" => $data
        ])->setStatusCode($code);
    }


    /**
     * This functions creates a JsonResponse from a customBaseException
     * @param CustomBaseException $customBaseException the custom base class exception
     *
     * @return JsonResponse the resulting response
     */
    public static function handleException(CustomBaseException $customBaseException): JsonResponse
    {
        if (count($customBaseException->getHttpErrorMessagesArr()) > 0) {
            return HTTPResponse::code(
                $customBaseException->getHttpStatus(),
                $customBaseException->getHttpErrorMessagesArr()
            );
        }
        return HTTPResponse::code($customBaseException->getHttpStatus(), [
            $customBaseException->getHttpMessage()
        ]);
    }


    /**
     * Returns a JsonResponse, that should be returned
     * Contains the 200 code, and a wrapper with the following structure:
     *
     * {
     *      "is_error": false,
     *      "data": $data
     * }
     * @param array $data The data that should be returned
     *
     * @return JsonResponse the resulting response
     */
    public static function success(array $data = [])
    {
        return HTTPResponse::code(200, $data);
    }


    /**
     * Returns a JsonResponse, that should be returned
     * Contains the 201 code, and a wrapper with the following structure:
     *
     * {
     *      "is_error": false,
     *      "data": $data
     * }
     * @param array $data The data that should be returned
     *
     * @return JsonResponse the resulting response
     */
    public static function created(array $data = [])
    {
        return HTTPResponse::code(201, $data);
    }


    /**
     * To be called when a resource is not found
     *
     * @param array|null $furtherData Additional data to be included in the response
     *
     * @return JsonResponse the resulting response
     */
    public static function resourceNotFound(array $furtherData = null)
    {
        $message = [
            "message" => "A resource you requested could not be found on the Server."
        ];

        if ($furtherData != null) {
            $message = ["furtherData" => $furtherData];
        }

        return HTTPResponse::code(404, $message);
    }


    /**
     * To be called when a user misses the rights to perform an action
     * Contains the 403 code
     *
     * @param array|null $furtherData Additional data to be included in the response
     *
     * @return JsonResponse the resulting response
     */
    public static function userMissesRights(array $furtherData = null)
    {
        $message = "You are missing the rights to perform this action.";

        if ($furtherData != null) {
            $message = ["furtherData" => $furtherData];
        }

        return HTTPResponse::forbidden($message, $furtherData);
    }


    /**
     * To be called when the user account does not match the account associated with a request
     * Contains the 403 code
     *
     * @param array|null $furtherData Additional data to be included in the response
     *
     * @return JsonResponse the resulting response
     */
    public static function accountMismatch(array $furtherData = null)
    {
        $message = "You are trying to access data unrelated to Your account. ";
        $message .= "You may only modify data related to Your account.";

        return HTTPResponse::forbidden($message, $furtherData);
    }


    /**
     * Aborts code execution whilst returning an account mismatch error
     *
     * @param array|null $furtherData The data to be included in the response
     *
     * @return void
     */
    public static function abortAccountMismatch(array $furtherData = null)
    {
        abort(HTTPResponse::accountMismatch($furtherData));
    }


    /**
     * To be called when the user account does not match have the rights to a request
     * Contains the 403 code
     *
     * @param string     $errorDescription an description of the error
     * @param array|null $furtherData      Additional data to be included in the response
     *
     * @return JsonResponse the resulting response
     */
    public static function forbidden(
        string $errorDescription = "You are forbidden to access this resource.",
        array $furtherData = null
    ): JsonResponse {
        $message["message"] = $errorDescription;
        if ($furtherData != null) {
            $message = ["furtherData" => $furtherData];
        }

        return HTTPResponse::code(403, $message);
    }


    /**
     * Aborts code execution whilst returning a forbidden error
     *
     * @param string|null $errorDescription an optional description of the error
     *
     * @return void
     */
    public static function abortForbidden(string $errorDescription = null)
    {
        abort(HTTPResponse::forbidden($errorDescription));
    }


    /**
     * To be called when a faulty / bad request is made
     * Contains the 400 code
     *
     * @param string|null $errorDescription an optional description of the error
     *
     * @return JsonResponse the resulting response
     */
    public static function badRequest(string $errorDescription = null, array $data = null): JsonResponse
    {
        $errorDescription = ["message" => $errorDescription ?? "You made a bad request", "further_data" => $data];
        return HTTPResponse::code(400, $errorDescription);
    }


    /**
     * Aborts code execution whilst returning a bad request error
     *
     * @param string|null $errorDescription an optional description of the error
     *
     * @return void
     */
    public static function abortBadRequest(string $errorDescription = null, array $data = null)
    {
        abort(HTTPResponse::badRequest($errorDescription, $data));
    }


    /**
     * To be called when a resource that would be created already exists
     * Contains the 303 code
     *
     * @param string|null $see A reference to the already existing resource
     *
     * @return JsonResponse the resulting response
     */
    public static function resourceAlreadyExists(string $see = null): JsonResponse
    {
        $message = ["message" => "You attempted to create a resource that already exists."];

        if ($see !== null) {
            $message["see"] = $see;
        }

        return HTTPResponse::code(303, $message);
    }


    /**
     * Returns an error for unexpected parameters within a request.
     *
     * @param array|null $furtherData Additional data to be included in the response
     *
     * @return JsonResponse the resulting response
     */
    public static function unexpectedParameter(array $furtherData = null): JsonResponse
    {
        $message = ["message" => "Unknown parameter inside request body."];
        if ($furtherData != null) {
            $message = array_merge($message, ["furtherData" => $furtherData]);
        }

        return HTTPResponse::code(400, $message);
    }


    /**
     * Aborts code execution whilst returning an unexpected parameter error
     *
     * @param array|null $furtherData Additional data to be included in the response
     *
     * @return void
     */
    public static function abortUnexpectedParameter(array $furtherData = null)
    {
        abort(HTTPResponse::unexpectedParameter($furtherData));
    }


    /**
     * Returns an error for unexpected parameters within a request.
     *
     * @param array|null $furtherData Additional data to be included in the response
     *
     * @return JsonResponse the resulting response
     */
    public static function malformedRequestBody(array $furtherData = null): JsonResponse
    {
        $message["message"] = "Malformed request body.";
        if ($furtherData != null) {
            $message = ["furtherData" => $furtherData];
        }

        return HTTPResponse::code(400, $message);
    }


    /**
     * Aborts code execution whilst returning a malformed request error
     *
     * @param array|null $furtherData Additional data to be included in the response
     *
     * @return void
     */
    public static function abortMalformedRequestBody(array $furtherData = null)
    {
        abort(HTTPResponse::malformedRequestBody($furtherData));
    }


    /**
     * Aborts code execution whilst returning an internal error
     *
     * @param string|null $errorDescription an optional description of the error
     *
     * @return void
     */
    public static function abortInternalError(string $errorDescription = null)
    {
        abort(HTTPResponse::serverError($errorDescription));
    }


    /**
     * Aborts code execution whilst returning a resource not found error
     *
     * @param string|null $errorDescription an optional description of the error
     *
     * @return void
     */
    public static function abortResourceNotFound(string $errorDescription = null)
    {
        abort(HTTPResponse::resourceNotFound($errorDescription));
    }


    /**
     * To be called when an unexpected state occurs. Should indicate critical errors
     *
     * @param string|null $description The error description
     *
     * @return JsonResponse the resulting response
     *
     */
    public static function serverError(string $description = null)
    {
        $furtherData["message"] = $description;
        return HTTPResponse::code(500, $furtherData);
    }
}

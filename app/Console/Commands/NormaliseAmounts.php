<?php

namespace App\Console\Commands;

use App\Models\Entry;
use Illuminate\Console\Command;

class NormaliseAmounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amounts:normalise';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entries = Entry::all();

        $entries->each(function($entry) {

            if ($entry->category->incoming) {
                $entry->amount = abs($entry->amount);
            }else {

                $entry->amount = -abs($entry->amount);
            }

            $entry->save();
        });
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class PortToNewCategoriesStructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:port_new_structure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Artisan::call("migrate:fresh");

        $accountsList = [];

        $accounts = DB::connection('mysql_old')->table('accounts')->get();
        foreach ($accounts as $account) {
            DB::table('accounts')->insert(
                [
                    "id" => $account->id,
                    "email" => $account->email,
                    "name" => $account->name,
                    "password_hash" => $account->password_hash,
                    "created_at" => $account->created_at,
                    "updated_at" => $account->updated_at,
                ]
            );


            $accountsList[$account->id] = Account::findOrFail($account->id);
        }

        $verifications = DB::connection('mysql_old')->table('pending_verifications')->get();
        foreach ($verifications as $verification) {
            DB::table('pending_verifications')->insert(
                [
                    "id" => $verification->id,
                    "account_id" => $verification->account_id,
                    "created_at" => $verification->created_at,
                    "updated_at" => $verification->updated_at,
                ]
            );
        }

        $tokens = DB::connection('mysql_old')->table('personal_access_tokens')->get();
        foreach ($tokens as $token) {

            DB::table('personal_access_tokens')->insert(
                [
                    "id" => $token->id,
                    "tokenable_type" => $token->tokenable_type,
                    "tokenable_id" => $token->tokenable_id,
                    "name" => $token->name,
                    "token" => $token->token,
                    "abilities" => $token->abilities,
                    "last_used_at" => $token->last_used_at,
                    "created_at" => $token->created_at,
                    "updated_at" => $token->updated_at,
                ]
            );
        }

        $tagsList = [];

        $tags = DB::connection('mysql_old')->table('tags')->get();
        foreach ($tags as $tag) {

            DB::table('tags')->insert(
                [
                    "id" => $tag->id,
                    "tag" => $tag->tag,
                    "color" => $tag->color,
                    "created_at" => $tag->created_at,
                    "updated_at" => $tag->updated_at,
                ]
            );

            $tagsList[$tag->id] = Tag::findOrFail($tag->id);
        }

        $categoriesPerAccountTable = [];
        // categories and entries

        $categories = DB::connection('mysql_old')->table('categories')->get();


        foreach ($accountsList as $account_id => $account) {

            $this->line($account_id . ": " . $account->email);

            $categoriesPerAccountTable[$account_id] = [];

            // maps original id to new category

            foreach ($categories as $category) {

                $accountCategories[$category->id] = Category::create(
                    [
                        "incoming" => $category->incoming,
                        "name" => $category->name,
                        "account_id" => $account_id,
                        "color" => $category->color,
                        "icon" => $category->icon
                    ]
                );
            }

            foreach ($categories as $category) {

                // map new parent ids
                // $this->line("Remapping: " . $category->name);

                if ($category->parent_identifier == null) {
                    continue;
                }

                $parent = DB::connection('mysql_old')
                    ->table('categories')
                    ->where("identifier", "=", $category->parent_identifier)
                    ->first();

                // $this->line("Parent: " . $parent->name);

                $accountCategories[$category->id]->parent_id =
                    $accountCategories[$parent->id]->id;
                $accountCategories[$category->id]->save();
            }
            $categoriesPerAccountTable[$account_id] = $accountCategories;
        }

        // entries:

        $entries = DB::connection('mysql_old')->table('entries')->get();

        foreach ($entries as $entry) {

            DB::table('entries')->insert(
                [
                    "id" => $entry->id,
                    "title" => $entry->title,
                    "description" => $entry->description,
                    "category_id" => $categoriesPerAccountTable[$entry->account_id][$entry->category_id]->id,
                    "account_id" => $entry->account_id,
                    "date" => $entry->date,
                    "amount" => $entry->amount,
                    "created_at" => $entry->created_at,
                    "updated_at" => $entry->updated_at,
                ]
            );
        }

        // tags:

        $linkTags = DB::connection('mysql_old')->table('link_tags_entries')->get();

        foreach ($linkTags as $link) {

            DB::table('link_tags_entries')->insert(
                [
                    "id" => $link->id,
                    "tag_id" => $link->tag_id,
                    "entry_id" => $link->entry_id,
                    "created_at" => $link->created_at,
                    "updated_at" => $link->updated_at,
                ]
            );
        }

        return 0;
    }
}

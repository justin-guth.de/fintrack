<?php

namespace App\Console\Commands;

use App\Models\Entry;
use App\Models\Tag;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class reset_tags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tags:rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('link_tags_entries')->delete();
        DB::table('tags')->delete();

        $allEntries = Entry::all();

        foreach($allEntries as $entry) {

            $this->line("Computing tags for " . $entry->title);

            $description = $entry->description;

            $tags = Tag::findIn($description);

            $line = "Found: ";

            foreach($tags as $tag) {

                $line .= $tag;
                $tObj = Tag::getOrBuild($tag);
                $tObj->linkEntry($entry->id);

            }

            $this->line($line);


        }

        return 0;
    }
}

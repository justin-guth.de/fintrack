<?php

namespace App\Http\Controllers;

use App\Mail\EmailConfirmationEmail;
use App\Models\Account;
use App\Models\PendingVerification;
use App\Utility\HTTPResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function register(Request $request)
    {

        $request->validate([
            'email' => 'required|unique:accounts|max:254|email|string',
            'name' => 'required',
            'password' => 'min:8|string|confirmed',
        ]);

        // if the validator does not fail we can create an account and create a verification entry.

        $data = $request->all();
        $password_hash = Hash::make($data["password"]);

        $account = Account::create(
            [
                "name" => $data["name"],
                "email" => $data["email"],
                "password_hash" => $password_hash,
            ]
        );

        $pendingVerification = PendingVerification::create(
            [
                "account_id" => $account->id,
            ]
        );

        Mail::to($account->email)
            ->send(
                new EmailConfirmationEmail(
                    $pendingVerification->id
                )
            );

        return HTTPResponse::created(["id" => $account->id]);
    }
}

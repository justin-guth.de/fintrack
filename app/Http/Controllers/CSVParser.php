<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Entry;
use App\Utility\HTTPResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CSVParser extends Controller
{
    public function parseEntryCsv(Request $request)
    {

        $account = Auth::user();
        $data = $request->all();

        $validator = Validator::make($data, [
            'data' => ['required', 'string'],
        ]);
        if ($validator->fails()) {

            HTTPResponse::abortBadRequest("The provided data was invalid!", ["failed" => $validator->failed()]);
        }

        $content = $data["data"];
        $lines = explode("\n", $content);

        $splitLines = [];

        foreach ($lines as $line) {

            $splitLine = explode(",", $line);

            if (count($splitLine) != 5) {

                HTTPResponse::abortBadRequest("Malformed csv file", [
                    "line" => $line
                ]);
            }

            $splitLines[] = $splitLine;
        }

        $rootId = Category::whereAccountId($account->id)
            ->where("parent_id", "=", null)->firstOrFail()->id;

        $existingCategories = Category::whereAccountId($account->id)
            ->get();

        $createdCategories = [];

        foreach ($existingCategories as $existingCategory) {

            $createdCategories[$existingCategory->name] = $existingCategory;
        }

        $results = [];

        foreach ($splitLines as $splitLine) {

            $categoryName = trim($splitLine[4]);

            if ($categoryName == "") {

                HTTPResponse::abortBadRequest("Malformed csv file: no category name provided", [
                    "line" => $splitLine
                ]);
            }


            if (!array_key_exists($categoryName, $createdCategories)) {

                $createdCategories[$categoryName] = Category::create(
                    [
                        "name" => $categoryName,
                        "color" => "#444444",
                        "parent_id" => $rootId,
                        "incoming" => true,
                        "account_id" => $account->id,
                    ]
                );
            }

            $entryDetails = [

                "title" => $splitLine[0],
                "date" => $splitLine[1],
                "amount" => $splitLine[2],
                "description" => trim($splitLine[3]) == "" ? null : trim($splitLine[3]),
                "category_id" => $createdCategories[$categoryName]->id,
            ];

            $validator = Validator::make($entryDetails, [
                'title' => ['required', 'string'],
                'amount' => ['required', 'integer'],
                'date' => ['required', 'date'],
                'category_id' => ['required', 'string', 'uuid'],
                'description' => ['string', "nullable"],
            ]);
            if ($validator->fails()) {

                HTTPResponse::abortBadRequest("The provided data was invalid!", ["line" => $splitLine, "failed" => $validator->failed()]);
            }

            if ($createdCategories[$categoryName]->incoming) {

                $entryDetails["amount"] = abs($entryDetails["amount"]);
            }
            else {
                $entryDetails["amount"] = -abs($entryDetails["amount"]);
            }

            $result = Entry::create(
                array_merge($entryDetails, [
                    "account_id" => $account->id
                ])
            );

            $result->recomputeTags();

            $results[] = $result->toArray();
        }

        return HTTPResponse::success(
            $results
        );
    }
}

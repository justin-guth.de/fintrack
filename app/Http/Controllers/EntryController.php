<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Entry;
use App\Models\Tag;
use App\Utility\CurrencyFormatter;
use App\Utility\HTTPResponse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class EntryController extends Controller
{
    public function getEntryList(Request $request)
    {
        $account = Auth::user();
        $data = [
            "begin" => $request->input("begin", null),
            "end" => $request->input("end", null),
        ];

        $validator = Validator::make($data, [
            'begin' => ['date', "nullable"],
            'end' => ['date', "nullable"],
        ]);
        if ($validator->fails()) {

            HTTPResponse::abortBadRequest("The provided data was invalid!", ["failed" => $validator->failed()]);
        }

        $fromDate = $request->input("begin", Carbon::createFromDate(1970, 1, 1));
        $toDate = $request->input("end", Carbon::now());

        $result = Entry::where("account_id", $account->id)
            ->where("date", "<=", $toDate)
            ->where("date", ">=", $fromDate)
            ->orderBy("date", "desc")
            ->get()->map(
                function ($entry) {
                    return $entry->toArray();
                }
            )->toArray();


        return HTTPResponse::success($result);
    }

    public function getEntry(string $id)
    {
        $account = Auth::user();

        $result = Entry::findOrFail($id);

        if ($result->account_id != $account->id) {

            HTTPResponse::abortAccountMismatch();
        }

        return HTTPResponse::success($result->toArray());
    }

    public function getLastNEntries($number)
    {
        $account = Auth::user();

        $result = DB::table('entries')
            ->where("entries.account_id", $account->id)
            ->orderBy("date", "desc")
            ->take($number)
            ->join("categories", "entries.category_id", "=", "categories.id")
            ->get(['entries.*', 'categories.name as category_name', "categories.color as color", "categories.icon as icon"])->map(
                function ($entry) {

                    // return $entry;

                    return [
                        "amount_representation" => CurrencyFormatter::formatEuro($entry->amount),
                        "category_id" => $entry->category_id,
                        "id" => $entry->id,
                        "title" => $entry->title,
                        "description" => $entry->description,
                        "date" => $entry->date,
                        "amount" => $entry->amount,
                        "absoluteAmount" => abs($entry->amount),
                        "account_id" => $entry->account_id,
                        "category_name" => $entry->category_name,
                        "category_color" => $entry->color,
                        "category_icon" => $entry->icon,
                    ];
                }
            )->toArray();


        return HTTPResponse::success($result);
    }

    public function addEntry(Request $request)
    {

        $account = Auth::user();
        $data = $request->all();

        $validator = Validator::make($data, [
            'title' => ['required', 'string'],
            'amount' => ['required', 'integer'],
            'date' => ['required', 'date'],
            'category_id' => ['required', 'string', 'uuid'],
        ]);
        if ($validator->fails()) {

            HTTPResponse::abortBadRequest("The provided data was invalid!", ["failed" => $validator->failed()]);
        }

        $category_id = $data["category_id"];

        try {
            Category::findOrFail($category_id);
        } catch (Exception $e) {
            HTTPResponse::abortBadRequest("Provided category ID not found!");
        }

        $category = Category::findOrFail($category_id);

        if ($category->incoming) {

            $data["amount"] = abs($data["amount"]);
        } else {
            $data["amount"] = -abs($data["amount"]);
        }

        $result = Entry::create(
            array_merge($data, [
                "account_id" => $account->id
            ])
        );

        $result->recomputeTags();

        return HTTPResponse::created($result->toArray());
    }

    public function deleteEntry(string $id)
    {

        $account = Auth::user();

        $entry = Entry::findOrFail($id);

        if ($entry->account_id != $account->id) {
            HTTPResponse::abortAccountMismatch();
        }

        $entry->delete();

        return HTTPResponse::success(["id" => $id]);
    }

    public function overwriteEntry(string $id, Request $request)
    {

        $account = Auth::user();

        $data = $request->all();

        try {
            Validator::make($data, [
                'title' => ['required', 'string'],
                'description' => ['string'],
                'amount' => ['required', 'integer'],
                'date' => ['required', 'date'],
                'category_id' => ['required', 'string', 'uuid'],
            ])->validate();
        } catch (Exception $e) {
            HTTPResponse::abortBadRequest("The provided data was invalid!");
        }

        $entry = Entry::with("category")->findOrFail($id);

        if ($entry->account_id != $account->id) {
            HTTPResponse::abortAccountMismatch();
        }



        $entry->title = $data["title"];
        $entry->description = $data["description"];
        $entry->date = $data["date"];
        $entry->category_id = $data["category_id"];

        $entry->save();
        $entry->refresh();

        if ($entry->category->incoming) {

            $entry->amount = abs($data["amount"]);
        } else {
            $entry->amount = -abs($data["amount"]);
        }




        $entry->recomputeTags();

        $entry->save();
        $entry->refresh();

        return HTTPResponse::success($entry->toArray());
    }

    public function overwriteEntryPartially(string $id, Request $request)
    {

        $account = Auth::user();

        $data = $request->all();

        try {
            Validator::make($data, [
                'title' => ['string'],
                'description' => ['string', "nullable"],
                'amount' => ['integer'],
                'date' => ['date'],
                'category_id' => ['string', 'uuid'],
            ])->validate();
        } catch (Exception $e) {
            HTTPResponse::abortBadRequest("The provided data was invalid", ["exception" => $e]);
        }

        $entry = Entry::with("category")->findOrFail($id);

        if ($entry->account_id != $account->id) {
            HTTPResponse::abortAccountMismatch();
        }



        $entry->title = $data["title"] ?? $entry->title;
        $entry->description = $data["description"] ?? $entry->description;
        $entry->amount = $data["amount"] ?? $entry->amount;
        $entry->category_id = $data["category_id"] ?? $entry->category_id;
        $entry->date = $data["date"] ?? $entry->date;

        $entry->save();
        $entry->refresh();

        if ($entry->category->incoming) {

            $entry->amount = abs($entry->amount);
        } else {
            $entry->amount = -abs($entry->amount);
        }


        $entry->recomputeTags();

        $entry->save();
        $entry->refresh();

        return HTTPResponse::success($entry->toArray());
    }
}

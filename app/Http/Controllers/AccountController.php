<?php

namespace App\Http\Controllers;

use App\Utility\HTTPResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function self() {

        return HTTPResponse::success(Auth::user()->getSelf());
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\PendingVerification;
use App\Utility\HTTPResponse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthenticationController extends Controller
{
    public function authenticate (Request $request) {

        $data = $request->all();

        $validator = Validator::make($data, [
            'email' => ['required', 'email'],
            'password' => ['required', 'string'],
            'device_name' => ['required', 'string'],
            'remember' => ['boolean'],
        ]);
        if($validator->fails()) {

            HTTPResponse::abortBadRequest("The provided data was invalid!", $validator->failed());
        }

        $account = Account::firstWhere("email", $data['email']);

        if ($account == null) {
            HTTPResponse::abortBadRequest("The provided data was invalid!");
        }

        $pwhash = $account->password_hash;

        if (!Hash::check($request->password, $pwhash)) {
            return HTTPResponse::badRequest('The provided credentials are incorrect.');
        }

        $pendingVerification = PendingVerification::firstWhere("account_id", $account->id);

        $pendingFlag = false;

        if ($pendingVerification != null) {

            $pendingFlag = true;
        }

        return HTTPResponse::created([
            "token" => $account->createToken($request->device_name)->plainTextToken,
            "pending_verification" => $pendingFlag,
        ]);
    }
}

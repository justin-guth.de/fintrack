<?php

namespace App\Http\Controllers;

use App\Utility\HTTPResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoryStatisticsController extends Controller
{
    public function getCategoryStatistics() {

        $account = Auth::user();

        $tags = DB::table("entries")
            ->select(
                'categories.id as id',
                'categories.name as name',
                'categories.parent_id as parent_id',
                'categories.color as color',
                DB::raw('CAST(SUM(entries.amount) as SIGNED) as amount')
                )
            ->where("entries.account_id", "=", $account->id)
            ->join("categories", "entries.category_id", "=", "categories.id")
            ->groupBy("categories.id")
            ->get();

        // TODO fix sorting
        $tags->sortBy("name");

        return HTTPResponse::success($tags->toArray());
    }
}

<?php

namespace App\Http\Controllers;

use App\Utility\HTTPResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DeficitStatisticsController extends Controller
{
    public function getDeficitStatistics()
    {

        $user = Auth::user();

        $income = DB::table("entries")
            ->select(DB::raw("SUM(amount) as income"))
            ->whereAccountId($user->id)
            ->where("amount", ">=", 0)
            ->get();

        $expenses = DB::table("entries")
            ->select(DB::raw("SUM(amount) as expenses"))
            ->whereAccountId($user->id)
            ->where("amount", "<", 0)
            ->get();

        return HTTPResponse::success([

            "income" => +$income[0]->income,
            "expenses" => +$expenses[0]->expenses,
            "deficit" => $income[0]->income + $expenses[0]->expenses,
        ]);
    }

    public function getRangedDeficitStatistics(string $begin, string $end)
    {

        $user = Auth::user();


        $income = DB::table("entries")
            ->select(DB::raw("SUM(amount) as income"))
            ->whereAccountId($user->id)
            ->where("date", "<=", Carbon::parse($end))
            ->where("date", ">=", Carbon::parse($begin))
            ->where("amount", ">=", 0)
            ->get();

        $expenses = DB::table("entries")
            ->select(DB::raw("SUM(amount) as expenses"))
            ->whereAccountId($user->id)
            ->where("date", "<=", Carbon::parse($end))
            ->where("date", ">=", Carbon::parse($begin))
            ->where("amount", "<", 0)
            ->get();

        return HTTPResponse::success([

            "income" => +$income[0]->income,
            "expenses" => +$expenses[0]->expenses,
            "deficit" => $income[0]->income + $expenses[0]->expenses,
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\Tag;
use App\Utility\HTTPResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TagStatisticsController extends Controller
{


    public function getTagStatistics()
    {

        $account = Auth::user();

        $tags = DB::table("entries")
            ->select('tags.id as id', 'tags.tag as tag', 'tags.color as color', DB::raw('CAST(SUM(entries.amount) as SIGNED) as amount'))
            ->where("entries.account_id", "=", $account->id)
            ->join("link_tags_entries", "entries.id", "=", "link_tags_entries.entry_id")
            ->join("tags as tags", "tags.id", "=", "link_tags_entries.tag_id")
            ->groupBy("id")
            ->get();

        return HTTPResponse::success($tags->toArray());



        //Tag::select('tag.tag','tag.color','SUM(entries.amount)')
        //->join("entries", "entries.id", "=","")

    }
}

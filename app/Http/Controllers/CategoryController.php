<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Utility\HTTPResponse;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function overwriteCategoryPartially(string $id, Request $request)
    {

        $account = Auth::user();

        $data = $request->all();

        try {
            Validator::make($data, [
                "parent_id" => ['uuid', "exists:categories,id"],
                'name' => ['string'],
                'color' => ["regex:/#(\d|[A-F]|[a-f]){6}/", "size:7"],
                "icon" => ["string", "nullable"]
            ])->validate();
        } catch (Exception $e) {
            HTTPResponse::abortBadRequest("The provided data was invalid", ["exception" => $e]);
        }

        $category = Category::findOrFail($id);

        if ($category->account_id != $account->id) {
            HTTPResponse::abortAccountMismatch();
        }



        $category->name = $data["name"] ?? $category->name;
        $category->color = $data["color"] ?? $category->color;
        $category->icon = $data["icon"] ?? $category->icon;
        $category->parent_id = $data["parent_id"] ?? $category->parent_id;
        $incoming = $category->incoming;

        if (array_key_exists("parent_id", $data)) {

            $parentId = $data["parent_id"];

            $parent = Category::findOrFail($parentId);

            $incoming = $parent->incoming;
            $category->incoming = $incoming;
        }

        $category->save();

        return HTTPResponse::success($category->toArray());
    }

    public function getCategory(string $id)
    {
        $account_id = Auth::id();
        $category = Category::findOrFail($id);

        if ($category->account_id != $account_id) {
            HTTPResponse::abortAccountMismatch();
        }

        return HTTPResponse::success($category->toArray());
    }

    public function getCategoryList()
    {
        $account_id = Auth::id();

        return HTTPResponse::success(Category::whereAccountId($account_id)->get()->toArray());
    }

    public function createCategory(Request $request)
    {
        $account = Auth::user();
        $data = $request->all();


        $validator = Validator::make($data, [
            "parent_id" => ['required', 'uuid', "exists:categories,id"],
            'name' => ['required', 'string'],
            'color' => ["regex:/#(\d|[A-F]|[a-f]){6}/", "size:7"],
            "icon" => ["string", "nullable"]

        ]);

        if ($validator->fails()) {

            HTTPResponse::abortBadRequest("The provided data was invalid!", ["failed" => $validator->failed()]);
        }

        $parentId = $data["parent_id"];

        $parent = Category::findOrFail($parentId);

        if ($parent->account_id != $account->id) {
            HTTPResponse::abortAccountMismatch();
        }

        $incoming = $parent->incoming;

        $data["name"] = trim($data["name"]);

        $result = Category::create(
            array_merge($data, [
                "incoming" => $incoming,
                "account_id" => $account->id,
                "parent_id" => $parent->id
            ])
        );

        return HTTPResponse::created($result->toArray());
    }

    public function getIndexedCategoryList()
    {
        $result = [];
        $account_id = Auth::id();

        foreach (Category::whereAccountId($account_id)->get()->toArray() as $element) {

            $result[$element["id"]] = $element;
        }

        return HTTPResponse::success($result);
    }

    public function getCategoryTree()
    {
        $account_id = Auth::id();
        $categories = Category::whereAccountId($account_id)->get();

        $roots = $categories->filter(function ($category) {
            return $category->parent_id == null;
        });

        function buildTree($node, $categories)
        {

            $children = $categories->filter(function ($category) use ($node) {
                return $category->parent_id == $node->id;
            })->map(function ($child) use ($categories) {
                return buildTree($child, $categories);
            })->toArray();

            return [
                "category" => $node->toArray(),
                "children" => array_values($children),
            ];
        }

        $tree = $roots->map(function ($root, $key) use ($categories) {
            return buildTree($root, $categories);
        })->toArray();

        return HTTPResponse::success(array_values($tree));
    }
}

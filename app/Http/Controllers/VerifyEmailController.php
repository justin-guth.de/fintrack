<?php

namespace App\Http\Controllers;

use App\Models\PendingVerification;
use App\Utility\HTTPResponse;
use Illuminate\Http\Request;

class VerifyEmailController extends Controller
{
    public function verify(string $uuid) {

        $pending = PendingVerification::findOrFail($uuid);

        $pending->delete();

        return HTTPResponse::success();
    }
}

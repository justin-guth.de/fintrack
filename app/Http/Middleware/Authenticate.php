<?php

namespace App\Http\Middleware;

use App\Utility\HTTPResponse;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // TODO @Justin seems hacky. But works for now.
        HTTPResponse::abortForbidden("You need to be authenticated to access this route.");
    }
}

<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class CustomBaseException extends Exception
{
    //
    protected int $httpStatus = 500;
    protected string $httpMessage = "An error occured. Please try again later.";
    protected array $httpErrorMessagesArr = [];


    /**
     * @return int
     */
    public function getHttpStatus(): int
    {
        return $this->httpStatus;
    }


    /**
     * @return string|array
     */
    public function getHttpMessage()
    {
        return $this->httpMessage;
    }


    /**
     * @return array
     */
    public function getHttpErrorMessagesArr(): array
    {
        return $this->httpErrorMessagesArr;
    }


}

# FinTrack

## CSV parsing

The provided csv string must provide the following fields:

| title                      | date       | amount | description   | category name |
| -------------------------- | ---------- | ------ | ------------- | ------------- |
| Birthday gift from grandma | 2021-10-01 | 5000   |               | Gifts         |
| Rent                       | 2021-10-02 | 35000  | Month october | Gifts         |

ergo the file has the following format:

```csv
Birthday gift from grandma,2021-10-01,5000,,Gifts
Rent,2021-10-02,35000,Month october,Gifts
```

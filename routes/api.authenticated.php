<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CategoryStatisticsController;
use App\Http\Controllers\CSVParser;
use App\Http\Controllers\DeficitStatisticsController;
use App\Http\Controllers\EntryController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TagStatisticsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("/self", [AccountController::class, "self"]);
Route::get("/category/{id}", [CategoryController::class, "getCategory"]);
Route::get("/categories/list", [CategoryController::class, "getCategoryList"]);
Route::get("/categories/tree", [CategoryController::class, "getCategoryTree"]);
Route::get("/categories/indexed", [CategoryController::class, "getIndexedCategoryList"]);
Route::get("/entries", [EntryController::class, "getEntryList"]);
Route::get("/entries/last/{n}", [EntryController::class, "getLastNEntries"]);
Route::post("/entry", [EntryController::class, "addEntry"]);
Route::delete("/entry/{id}", [EntryController::class, "deleteEntry"]);
Route::put("/entry/{id}", [EntryController::class, "overwriteEntry"]);
Route::patch("/entry/{id}", [EntryController::class, "overwriteEntryPartially"]);
Route::get("/entry/{id}", [EntryController::class, "getEntry"]);
Route::get("/statistics/tags", [TagStatisticsController::class, "getTagStatistics"]);
Route::get("/statistics/categories", [CategoryStatisticsController::class, "getCategoryStatistics"]);
Route::get("/statistics/deficit", [DeficitStatisticsController::class, "getDeficitStatistics"]);
Route::get("/statistics/deficit/{begin}/{end}", [DeficitStatisticsController::class, "getRangedDeficitStatistics"]);
Route::post("/category", [CategoryController::class, "createCategory"]);
Route::patch("/category/{id}", [CategoryController::class, "overwriteCategoryPartially"]);
Route::post("/csv/entries", [CSVParser::class, "parseEntryCsv"]);


<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post("/register", [RegisterController::class, "register"]);
Route::patch('/verify_email/{uuid}', [VerifyEmailController::class, "verify"]);
Route::post('/authenticate', [AuthenticationController::class, "authenticate"]);
Route::post("/migrate", function(Request $request) {

    return Artisan::call("migrate");
});

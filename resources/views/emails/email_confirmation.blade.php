@component('mail::message')
# Hello,

Please verify your email adress by clicking the link or pasting it in your browser.

@component("mail::button", ["url" => $url])
    Verify your e-mail adress
@endcomponent

If the button does not work please copy and paste the link into your browser.
[{{$url}}]({{$url}})

Kind regards,

Fintrack

@endcomponent
